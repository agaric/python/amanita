Feature: Baseline functionality works

  @api
  Scenario: Make sure that when I load the homepage the request succeeds.
    Given I am on "/"
    Then I should get a 200 HTTP response
