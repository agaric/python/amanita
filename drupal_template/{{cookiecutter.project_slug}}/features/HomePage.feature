Feature: Homepage Feature

  @api @javascript
  Scenario: Check for content in sections on home page
    Given I am logged in as a user with the "Authenticated" role
    When I am on "/"
    Then I should see the heading "Welcome!"
