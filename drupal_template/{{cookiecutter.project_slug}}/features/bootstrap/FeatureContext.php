<?php

use Behat\Mink\Exception\UnsupportedDriverActionException;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Mink\Exception\ElementNotFoundException;
use WebDriver\Exception;
use Drupal\Core\Database\Database;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext {

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * @Then I should not visibly see the element :selector
   */
  public function assertElementNotVisible($selector) {
    $element = $this->getSession()->getPage();
    $result = $element->find('css', $selector);

    try {
      if ($result && $result->isVisible()) {
        throw new \Exception(sprintf("Selector '%s' is visible on the page %s", $selector, $this->getSession()->getCurrentUrl()));
      }
    }
    catch (UnsupportedDriverActionException $e) {
      // We catch the UnsupportedDriverActionException exception in case
      // this step is not being performed by a driver that supports javascript.
      // All other exceptions are valid.
    }
  }

  /**
   * @Given I wait :time seconds
   */
  public function iWait($time) {
    $this->getSession()->wait(1000 * $time);
  }

  /**
   * @When I choose :option from the :selector select
   */
  public function iChooseFromTheSelect($option, $selector) {
    $element = $this->getSession()->getPage();
    $result = $element->find('css', $selector);
    if (!$result || !$result->isVisible()) {
      throw new \Exception(sprintf("Selector '%s' is not on the page or is not visible", $selector));
    }

    $result->selectOption($option);
  }

  /**
   * @When I click the text :label
   */
  public function iClickTheText($label_text) {
    $page = $this->getSession()->getPage();
    $label_elements = $page->findAll('named_exact', ['content', $label_text]);
    if (count($label_elements) === 0) {
      throw new ElementNotFoundException($this->getSession()->getDriver(), 'content', 'named_exact', $label_text);
    }
    foreach ($label_elements as $label_element) {
      try {
        $label_element->click();
        return;
      }
      catch (Exception $e) {
        continue;
      }
    }
  }

  /**
   * @When I click on the element :elementCssSelector
   */
  public function clickOnElement($elementCssSelector) {
    $session = $this->getSession();
    $element = $session->getPage()->find('css', $elementCssSelector);

    if (NULL === $element) {
      throw new \Exception("Element not found: " . $elementCssSelector);
    }

    // Click on the element
    $element->click();
  }

  /**
   * Input text using CKEditor.
   *
   * @Then I fill in the wysiwyg field :css_selector with :value
   * @Then I enter :value for wysiwyg field :css_selector
   */
  public function iFillInWysiwygOnFieldWith($css_selector, $value) {
    # https://ckeditor.com/docs/ckeditor5/latest/support/faq.html#how-to-get-the-editor-instance-object-from-the-dom-element
    $this->getSession()
      ->executeScript(
        "
        let ckeditorInstance = CKEDITOR.instances[\"$css_selector\"]
        if (ckeditorInstance) {
          ckeditorInstance.setData(\"$value\");
        } else {
          throw new Error('Could not get the editor instance!');
        }
        ");
  }

  /**
   * @Then I should be redirected to a known target page
   */
  public function iShouldBeRedirectedToAKnownTargetPage() {
    $currentUrl = $this->getSession()->getCurrentUrl();
    if (strpos($currentUrl, '/target-page') === FALSE) {
      throw new \Exception('Not on the expected target page');
    }
  }

  /**
   *
   * @When I truncate all tables for :bundles
   */
  public function cleanUpAfterScenario($bundles_string) {
    $bundles = explode(',', str_replace(' ', '', $bundles_string));
    $bundles = [
      'node' => $bundles,
      'media' => ['image', 'pdf'],
      'taxonomy_term' => ['tags', 'kyr_topics', 'visas'],
    ];

    $base_tables = [
      'node',
      'node_field_data',
      'node_revision',
      'node_field_revision',
      'path_alias',
      'path_alias_revision',
      'media',
      'media_field_data',
      'media_revision',
      'media_field_revision',
      'taxonomy_term_data',
      'taxonomy_term_field_data',
      'taxonomy_term_revision',
      'taxonomy_term_field_revision',
      'taxonomy_term__parent',
      'taxonomy_term_revision__parent',
      'file_managed',
      'file_usage',
      'comment',
      'comment_entity_statistics',
      'comment_field_data',
      'comment__comment_body'
    ];

    foreach ($base_tables as $table) {
      exec("drush sql-query 'TRUNCATE $table;'");
      # For tables that are confirmed to have an identity field (need to determine): exec("drush sql-query 'ALTER TABLE $table AUTO_INCREMENT = 1'");
    }

    $this->truncateSerialTables();

    foreach ($bundles as $entity_type => $bundle_group) {
      foreach ($bundle_group as $bundle) {
        $this->truncateFieldTables($entity_type, $bundle);
      }
    }
    exec("drush cr");
    echo "After scenario cleanup: relevant tables truncated!";
  }

  /**
   * Truncate field and revision tables for a specific bundle.
   *
   * @param string $entity_type
   * @param string $bundle
   */
  protected function truncateFieldTables($entity_type, $bundle) {
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $field_configs = $entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    foreach ($field_configs as $field_config) {
      if ($field_config instanceof Drupal\field\Entity\FieldConfig) {
        $field_name = $field_config->getName();

        $field_table = "{$entity_type}__$field_name";
        $revision_table = "{$entity_type}_revision__$field_name";

        exec("drush sql-query 'TRUNCATE $field_table;'");
        exec("drush sql-query 'TRUNCATE $revision_table;'");
        # For tables that are confirmed to have an identity field (need to determine): exec("drush sql-query 'ALTER TABLE $field_table AUTO_INCREMENT = 1'"); (+revision)
      }
    }
  }
  protected function truncateSerialTables() {
    $connection = Database::getConnection();

    $query = $connection->query("SELECT table_name FROM information_schema.tables WHERE table_name LIKE 'serial_%' AND table_schema = :schema", [
      ':schema' => $connection->getConnectionOptions()['database'],
    ]);

    $tables = $query->fetchAll();
    foreach ($tables as $table) {
      $table_name = $table->table_name;
      $connection->query("TRUNCATE TABLE {$table_name}");
      echo "Truncated table: {$table_name}\n";
    }
  }
}
