Feature: Search works

  @api
  Scenario: The search page loads and a garbage query returns no results
    Given I am on "/search"
    Then I should see "Search"
    And for "Search" I enter "asdfbjijij"
    And I press "Search"
    And I should see "no results"

#  TODO: Real test searches with baseline sample data.
