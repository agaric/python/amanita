
# Syncing live/test sites locally

```shell
$ ddev auth ssh
$ ddev drush sql-sync @live @self
$ ddev drush rsync --exclude-paths=/php:/js:/css:/twig:/styles @live:%files @self:%files 
```
