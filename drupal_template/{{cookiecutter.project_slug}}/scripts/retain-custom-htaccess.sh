#!/usr/bin/env bash

if [ -f ./web/.htaccess.custom ]
then
    cd web && mv .htaccess .htaccess.default && cp .htaccess.custom .htaccess
    git diff -s --exit-code .htaccess.default || echo 'NOTE: .htaccess.default file has changed! Be sure to integrate latest updates into .htaccess and .htaccess.custom, then commit all three files. For example, meld web/.htaccess.custom web/.htaccess.default'
fi
