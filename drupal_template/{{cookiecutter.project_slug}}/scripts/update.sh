#!/usr/bin/env bash
set -euo pipefail
# ~SCRIPT~ update.sh
# * Pass in a path to drush, or use a default:
# * Runs post-deploy commands to update Drupal

# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
DIR="$( dirname -- "${BASH_SOURCE[0]}"; )";   # Get the directory name
SCRIPT_DIR="$( realpath -e -- "$DIR"; )";     # Resolve its full path if need be

drush=${*-${SCRIPT_DIR}/../vendor/bin/drush}
echo "Using drush=${drush}"

_oldpath=$PATH
export PATH=$HOME/bin:/usr/local/bin:/usr/bin:/bin

echo ">> [D] Running drush deploy"
${drush} deploy
export PATH=$_oldpath
exit 0
