#!/usr/bin/env bash
set -euo pipefail
# Confirmed with echo `pwd` that as long as we use via composer it's always in
# /var/www/html (aka the project root).
proj_dir="$(cd $(dirname ${0})/../ && pwd)" # Parent of this script folder.
[ "$proj_dir" != "/var/www/html" ] && { echo "Script running from unexpected path - are you running within ddev, as you should?  Try ddev composer css"; exit 1; }
cd /var/www/html/web/themes/custom/{{cookiecutter.theme_name}}
# Yarn CSS only
yarn node-sass -r ./src --output ./dist --include-path node_modules --include-path src --output-style compressed --quiet
yarn postcss ./dist/{**,.,*}.css --replace --verbose --env production
