#!/bin/env bash
set -e
ddev composer require --dev behat/behat drupal/drupal-extension
ddev get ddev/ddev-selenium-standalone-chrome
echo "Restart ddev to add selenium container"

