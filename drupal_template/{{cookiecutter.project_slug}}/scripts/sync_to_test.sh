#!/usr/bin/env bash
set -euo pipefail

source_site_web="{{cookiecutter.liveuser_webdir}}/"
source_site_root="{{cookiecutter.liveuser_sitedir}}/"
source_site_vendor="{{cookiecutter.liveuser_sitedir}}/vendor/"
source_drush="{{cookiecutter.liveuser_sitedir}}/vendor/bin/drush -r ${source_site_web}"
source_files="${source_site_web}/sites/default/files/"
source_backups="{{cookiecutter.liveuser_backupdir}}"

# Requires an .ssh/config entry for test-site. May be to localhost, and must not require a password for private key.
target_site={{"test-site" if cookiecutter.sync_method == "ssh"}}
target_site_web="{{cookiecutter.testuser_webdir}}/"
target_site_root="{{cookiecutter.testuser_sitedir}}/"
target_site_vendor="{{cookiecutter.testuser_sitedir}}/vendor/"
target_drush="{{cookiecutter.testuser_sitedir}}/vendor/bin/drush -r ${target_site_web}"
target_files="${target_site_web}/sites/default/files/"
target_backups="{{cookiecutter.testuser_backupdir}}"

# Use ssh to target when target_site is set, otherwise use eval to just run in the local context
target_cmd="${target_site:+ssh ${target_site}}"
target_cmd="${target_cmd:-eval}"

sync_date=$(date +%Y%m%d_%H%M%S%Z)

# Test local drush/file paths/ssh connection/remote drush
[ -d ${source_files} ] || (echo "source_files folder does not exist"; exit 1)
${source_drush} status > /dev/null || (echo "Cannot execute local drush"; exit 1)
${target_cmd} "[ -d ${target_files} ]" || (echo "${target_cmd} failed, or ${target_files} folder does not exist at target. If using ssh, have you set up the key?"; exit 1)
${target_cmd} "${target_drush} status > /dev/null" || (echo "${target_cmd} ${target_drush} failed - does the site exist"; exit 1)

echo ">> All checks succeeded!"
echo ">> Syncing this site to ${target_site:-${target_site_web}} (using file date: ${sync_date})..."

# Dump destination DB (extra backup)
echo ">> Dumping destination database..."
${target_cmd} "${target_drush} sql-dump | gzip > ${target_backups}/${sync_date}-paranoia.sql.gz"
echo ">> Dumping source database..."
${source_drush} sql-dump {{"| gzip " if cookiecutter.sync_method == "ssh"}} > ${source_backups}/${sync_date}-sync.sql{{".gz" if cookiecutter.sync_method == "ssh"}}

# Drop DB and reload from source
echo ">> Dropping target database..."
${target_cmd} "${target_drush} sql-drop -y"

{% if cookiecutter.sync_method == 'ssh' %}
# Send copy (note: selectively gzipped on sql-dump above)
echo ">> Copying source db to target..."
scp ${source_backups}/${sync_date}-sync.sql.gz ${target_site}:${target_backups}/
${target_cmd} "gunzip ${target_backups}/${sync_date}-sync.sql.gz"
# Reload from source copy on target
echo ">> Reloading target database..."
${target_cmd} "${target_drush} sqlc < ${target_backups}/${sync_date}-sync.sql"
{% else %}
# Reload directly from source backup
echo ">> Reloading target database..."
${target_cmd} "${target_drush} sqlc < ${source_backups}/${sync_date}-sync.sql"
{% endif %}

# Copy files
echo ">> Sending new files..."
rsync -rlptvz --exclude /php --exclude /js --exclude /css --exclude /styles --exclude /twig ${source_files} ${target_site:+${target_site}:}${target_files}

if [ "${1:-}" = "code" ]; then
  echo ">> Synchronizing live code & config with test instance"
  rsync -rvz --delete --links ${source_site_vendor} ${target_site:+${target_site}:}${target_site_vendor}
  rsync -rvz --delete --exclude .well-known --exclude sites/default/files --exclude sites/default/settings.*.php ${source_site_web} ${target_site:+${target_site}:}${target_site_web}
  rsync -rvz --delete ${source_site_root}config/ ${target_site:+${target_site}:}${target_site_root}config
  {% if cookiecutter.sync_method == 'ssh' %}
  scp {source_site_root}composer.json ${target_site:+${target_site}:}${target_site_root}
  {% else %}
  cp {source_site_root}composer.json ${target_site_root}
  {% endif %}
fi

# CR forever!
${target_cmd} "${target_drush} cr"

echo ">> Target ${target_site:-${target_site_web}} is ready!"
