#!/usr/bin/env bash
set -euo pipefail
# ~SCRIPT~ update.sh
# * Pass in a path to drush, or use a default:
# * Runs post-deploy commands to update Drupal
# ~REQUIRED~
# BUILD_ROOT:
#   Where to do the build, no default
#
# ~OPTIONAL~
# COMPOSER_ARGS:
#   Composer command and flags to run
#   Defaults to "install --no-dev --no-scripts --optimize-autoload"

# Load env vars, if supplied
[ $# -eq 1 ] && source "env-${1}"

# Complain/stop if we don't have needed values
[ -z ${BUILD_ROOT:-} ] && { echo ">> Missing ENV var: BUILD_ROOT"; exit 1 ; }

# Set defaults
[ -z ${COMPOSER_ARGS:- } ] && COMPOSER_ARGS="install --no-dev --no-scripts --optimize-autoloader"

# Build dir has to exist first (if only for a moment)...
[ ! -d ${BUILD_ROOT} ] && { mkdir -p ${BUILD_ROOT} || true ; }

# Normalize paths (e.g. "./build" => "/project/thisone/build)":
BUILD_ROOT="$(cd "${BUILD_ROOT}" && pwd)"
proj_dir="$(cd $(dirname ${0})/../ && pwd)" # Parent of this script folder.

# Wipe out the build dir, so long as it is somewhere OTHER than the project root.
if [ "${BUILD_ROOT}" != "${proj_dir}" ] ; then
    # wipe out existing target, and get latest code.
    rm -rf ${BUILD_ROOT}
    mkdir ${BUILD_ROOT}
    git archive HEAD | tar -x -C ${BUILD_ROOT}
    cd ${BUILD_ROOT}
    git describe --always --long --all --abbrev=40 HEAD > VERSION
    composer ${COMPOSER_ARGS}
fi
