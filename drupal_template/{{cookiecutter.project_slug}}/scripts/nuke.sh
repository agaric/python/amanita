# Delete Drupal core and any contrib directories.
rm -rf {{cookiecutter.webdir}}/core
rm -rf {{cookiecutter.webdir}}/modules/contrib
rm -rf {{cookiecutter.webdir}}/themes/contrib
rm -rf {{cookiecutter.webdir}}/profiles/contrib
rm -rf drush/Commands/contrib
 
# Delete Drupal root files.
rm -f {{cookiecutter.webdir}}/.csslintrc
rm -f {{cookiecutter.webdir}}/.editorconfig
rm -f {{cookiecutter.webdir}}/.eslintignore
rm -f {{cookiecutter.webdir}}/.eslintrc.json
rm -f {{cookiecutter.webdir}}/.gitattributes
rm -f {{cookiecutter.webdir}}/.gitignore
rm -f {{cookiecutter.webdir}}/.ht.router.php
rm -f {{cookiecutter.webdir}}/autoload.php
rm -f {{cookiecutter.webdir}}/example.gitignore
rm -f {{cookiecutter.webdir}}/index.php
rm -f {{cookiecutter.webdir}}/INSTALL.txt
rm -f {{cookiecutter.webdir}}/robots.txt
rm -f {{cookiecutter.webdir}}/README.txt
rm -f {{cookiecutter.webdir}}/update.php
rm -f {{cookiecutter.webdir}}/web.config
 
# Delete any Drupal scaffold files.
rm -f .editorconfig
rm -f .gitattributes
 
# Delete any third party libraries.
# rm -rf {{cookiecutter.webdir}}/libraries
 
# Delete the vendor directory.
rm -rf vendor

# Delete any build folders.
rm -rf build
