#!/usr/bin/env bash
set -e
backup_dir=${BACKUP_DIR:-}
db_backup_days=${DB_BACKUP_DAYS:-5}
[ -z "${DEPLOY_ENV:-}" ] && DEPLOY_ENV="${CI_ENVIRONMENT_NAME:-}"
[ -z "${DEPLOY_ENV:-}" ] && { echo 'Missing ENV var: CI_ENVIRONMENT_NAME/DEPLOY_ENV'; exit 1; }
[ -z "${DEPLOY_ROOT:-}" ] && { echo "Missing ENV var: BACKUP_DIR"; exit 1; }
[ -z "${SSH_TARGET:-}" ] && { echo "Missing ENV var: SSH_TARGET"; exit 1; }
# delete old backups
ssh_cmd="find ${backup_dir} -maxdepth 1 -name '${DEPLOY_ENV}_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]T[0-9][0-9][0-9][0-9][0-9][0-9].sql.gz\' -mtime +${db_backup_days} -exec rm {} \;"
ssh "${SSH_TARGET}" "$ssh_cmd"
