## System Housekeeping (innoc[ulate])

***Coming soon!***

- ground [plant|...]

amanita innoculate system - install the apps we all use: docker, ddev, composer, code-oss|vscodium, etc.

Reference:

 * https://docs.agaric.coop/project-management-with-gitlab.html
 * https://docs.agaric.coop/tools/git-setup.html

Required software:

 * nextcloud-client
 * git
 * rsync
 * ssh
 * ddev
 * docker
 * composer
 * ansible
 * code-oss|vscodium|vscode
 * ahoy

Optional software:

 * zulip-desktop
 * meld

## Hosting (mycelium)

***Coming soon!***

 * `amanita mycelium spread` - configure a hosting instance (i.e. add drutopia_hosting_template)
 * `amanita mycelium grow` - configure a hosting user (near equivalent to ahoy new-site)

Prior art:
drutopia/hosting/hosting_private/.ahoy.yml - ahoy new-site

## Drupal (fruit)

amanita fruit - create a new basic drupal site
*amanita fruit drutopia* - create a new drutopia site

Prior art:
https://docs.agaric.coop/tools/creating-new-drutopia-site.html

### Maintenance (rain)

amanita rain - e.g. update composer.*
amanita rain upwards - meld/compare composer.*

https://docs.agaric.coop/tools/deploying-drutopia-updates.html
