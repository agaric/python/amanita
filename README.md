# amanita

Agaric's in-house helper - configure your system, create sites, and so on.

# Installation

First get it and put it in your local projects directory:

```bash
cd ~/Projects
git clone git@gitlab.com:agaric/python/amanita.git
cd amanita
```

On debian, arch, etc, use: `./linux-install.sh`

For NixOS, add `just`, and `cruft`.

On bash/zsh, add an alias for your system (to e.g. `~/.bash_aliases`), which runs just, supplying the justfile in this folder. For example:
`alias amanita=just --justfile=/home/agaric-worker/projects/amanita/justfile`

For the fish shell, you can update your aliases by running:
`alias --save amanita 'just --justfile=/home/agaric-worker/projects/amanita/justfile'`

Should everything succeed, reloading your shell should provide you with an `amanita` command to run.

## Usage

See what prompts are available:

```bash
amanita
```

| command | help |
| --- | --- |
|amanita ddev |                 Run the standard ddev coniguration command|
|amanita fruit |                Create a Drupal project from the standard template (in a subfolder of the working directory) |
|amanita fruit-link-existing |  Update an existing project from template |
|amanita harvest |              Pull changes and prompt for updating private variables |
|amanita hello |                Ensure your environment is configured for amanita |
|amanita update_composer |      Updates project composer files |
|amanita mf-ssh-preauth instance |      Pre-authenticate to Mayfirst for the specified instance |

Note that presently, the fruit-link-existing is a bit brittle. Run it from the parent folder of your target, specifying just the folder name to copy the resulting tempated files into. e.g. `just fruit-link-existing myclient`, where `myclient` contains your `composer.json`, `.ddev` folder, etc.

If you would like to skip updates to certain files after the first run, you can do so by editing the `.cruft.json` file:
```json
{
   "template": "git@gitlab.com:agaric/python/amanita.git",
   "commit": "2af33ab6101ef83f0036864a23d3660beb9d680a",
   "skip": [
      ".gitlab-ci.yml",
      "drush/sites/self.site.yml"
   ],
   ...
```

Also see [cruft usage](https://cruft.github.io/cruft/) for more options when performing updates (typically covered by amanita harvest).

# Variables

{
  "project_name": "Drupal10", # typically the client name with no special characters. Used for e.g. ddev project name
  "project_slug": "{{ cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_') }}", # should match the desired client folder name
  "composer_source": ["drutopia", "drutopia_dev_tempate", "standard", "none"], # when using just composer, where to pull a composer file from.
  "hostname": "example.com", # the 'root' of the host name (e.g. example.com subsequently used to guess test.example.com for test)
  "live_uri": "https://{{cookiecutter.hostname}}", # the live uri
  "test_uri": "https://test.{{cookiecutter.hostname}}", # the test uri
  "user": "{{cookiecutter.project_slug}}", # the 'root' remote user name (e.g. client is then used to guess client_live for live, etc)
  "test_user": "{{cookiecutter.user}}_test", # the name of the test user
  "live_user": "{{cookiecutter.user}}_live", # the name of the live user
  "sitedir":"site", # the root folder, which contains composer.json/web/etc.
  "webdir":"web", # just the subfolder of the sitedir which corresponds to drupal root
  "testuser_homedir":"/home/{{cookiecutter.test_user}}", # the test user's home folder
  "liveuser_homedir":"/home/{{cookiecutter.live_user}}", # the live user's home folder
  "testuser_sitedir": "{{cookiecutter.testuser_homedir}}/{{cookiecutter.sitedir}}", # as per site-dir, but specific to the test user
  "liveuser_sitedir": "{{cookiecutter.liveuser_homedir}}/{{cookiecutter.sitedir}}", # as per site-dir, but specific to the live user
  "testuser_webdir": "{{cookiecutter.testuser_sitedir}}/{{cookiecutter.webdir}}", # as per web-dir, but specific to the live user
  "liveuser_webdir": "{{cookiecutter.liveuser_sitedir}}/{{cookiecutter.webdir}}", # as per web-dir, but specific to the test user
  "theme_name": "theme", # the site theme, only used to configure scripts/css.sh
  "sync_method": ["ssh", "local"], # how to sync the site: either a local copy, or via ssh
  "backupdir": "backups" # relative to the user's home folder, the path of the backup folder
  "testuser_backupdir": "{{cookiecutter.testuser_homedir}}/{{cookiecutter.backupdir}}", # as per backupdir, but specific to the live user
  "liveuser_backupdir": "{{cookiecutter.liveuser_homedir}}/{{cookiecutter.backupdir}}", # as per backupdir, but specific to the test user
}
