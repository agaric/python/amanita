#!/usr/bin/env bash
# Installs:
# - ahoy
# - cruft (cookiecutter) [using pipx, pip3, or pip, in that order]
# - ~/.ahoy.yml (include amanita commands)
set -euo pipefail
# Install ahoy
latest_cruft="2.15.0"

os="unknown"
case "$(uname -s)" in
  Linux*)     os="Linux";;
  Darwin*)    os="Mac";;
  CYGWIN*|MINGW*|MSYS*|*_NT*) os="Windows";;
esac
echo "Detected OS is: $os"
if [ "${os}" != "Linux" ]; then
  echo "Sorry, this only works for linux presently. See https://github.com/ahoy-cli/ahoy to install ahoy"
  exit 1
fi

## Install/update ~~ahoy~~ JUST
#install_ahoy=0
#if ! (command -v ahoy &> /dev/null); then
#  echo "ahoy not found"
#  install_ahoy=1
#elif [ "$(ahoy --version)" != "${latest_ahoy}" ]; then
#  echo "ahoy is not the expected version"
#  install_ahoy=1
#else
#  echo "ahoy installed and correct version"
#fi
#if [ $install_ahoy -eq 1 ]; then
#  echo ">> downloading ahoy to /usr/local/bin/ahoy. This will require root"
#  os=$(uname -s | tr [:upper:] [:lower:])
#  architecture=$(case $(uname -m) in x86_64 | amd64) echo "amd64" ;; aarch64 | arm64 | armv8) echo "arm64" ;; *) echo "amd64" ;; esac)
#  sudo wget -q https://github.com/ahoy-cli/ahoy/releases/latest/download/ahoy-bin-$os-$architecture -O /usr/local/bin/ahoy && sudo chown $USER /usr/local/bin/ahoy && chmod +x /usr/local/bin/ahoy
#fi

# Install/update cruft
install_cruft=""
pip=""
if ! (command -v cruft &> /dev/null); then
  install_cruft="install"
elif [ "$(cruft --help | grep 'Version:' | sed 's/ Version: //g' | tr -d ' ')" != "${latest_cruft}" ]; then
  echo "cruft is not the expected version"
  install_cruft="install -U"
else
  echo "cruft installed and is correct version"
fi
if ! [ -z "${install_cruft:-}" ]; then
  # Determine install method: pipx, pip3, then pip
  if (command -v pipx &> /dev/null); then
    pip=pipx
  elif (command -v pip3 &> /dev/null); then
    pip=pip3
  elif (command -v pip &> /dev/null); then
    pip=pip
  else
    echo "None of pipx|pip3|pip found - not installing requirement: cruft"
  fi
  if ! [ -z "${pip:-}" ]; then
    if [ "${pip}" = "pipx" ]; then
      echo ">> install (or updating) cruft with 'pipx install cruft --force'"
      pipx install cruft --force
    else
      echo ">> installing (or updating) cruft with '${pip} ${install_cruft} cruft'"
      ${pip} ${install_cruft} cruft
    fi
  fi
fi

## Examine current ~/.ahoy.yml
#if [ -f ~/.ahoy.yml ]; then
#  if (grep -q "amanita.yml" ~/.ahoy.yml); then
#    echo "~/.ahoy.yml exists and appears to refer to amanita.yml"
#    echo "This is no longer necessary: you may remove this reference."
#  fi
#fi
#if [ -z "${AMANITA_DIR:-}" ]; then
#	echo "Be sure to source amanita.sh from your bash profile or rc file (AMANITA_DIR not set)"
#fi
# ...maybe do something to automatically update aliases? Or, just echo the line to add to an alias file.
