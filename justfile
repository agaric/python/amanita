
#first command is treated as the default command
default:
  @just --list --unsorted

# Create a Drupal project from the standard template (in a subfolder of the working directory)
[no-cd]
fruit:
  cruft create --directory drupal_template git@gitlab.com:agaric/python/amanita.git
  @echo "Project created! cd into it and run \`amanita update-composer\` to get composer sources"
  @echo "Then run amanita ddev"

# Update an existing project from template
[no-cd]
fruit-link-existing target-dir:
  #!/usr/bin/env sh
  set -euo pipefail
  tmp=$(mktemp -d)
  cd $tmp
  cruft create --output-dir $tmp --directory drupal_template git@gitlab.com:agaric/python/amanita.git && rsync -av $tmp/ {{ target-dir }}/..
  echo "Project updated! Run \`amanita update-composer\` to get composer sources"

# Pull changes and prompt for updating private variables
[no-cd]
harvest:
  cruft update -i
  @echo "Project updated! Run \`amanita update-composer\` to get composer sources"

# Run the standard ddev coniguration command
[no-cd]
ddev:
  #!/usr/bin/env sh
  [ -f .cruft.json ] || (echo "No .cruft.json in $PWD"; exit 1)
  web=$(jq -r '.context.cookiecutter.webdir' .cruft.json)
  proj=$(jq -r '.context.cookiecutter.project_name' .cruft.json)
  ddev config --docroot=$web --webserver-type=apache-fpm --database=mariadb:10.5 --php-version=8.2 --create-docroot --project-type=drupal10 --project-name=$proj

# Updates project composer files
[no-cd]
update-composer:
  #!/usr/bin/env sh
  [ -f .cruft.json ] || (echo "No .cruft.json in $PWD"; exit 1)
  which_composer=$(jq -r '.context.cookiecutter.composer_source' .cruft.json)
  if [ "${which_composer}" == "drutopia" ]; then
    # Update drutopia source...
    echo "Updating drutopia composer files"
    curl --output composer.json https://gitlab.com/drutopia-platform/build_source/-/raw/main/composer.json
    curl --output composer.lock https://gitlab.com/drutopia-platform/build_source/-/raw/main/composer.lock
  elif [ "${which_composer}" == "drutopia_dev_tempate" ]; then
    # Update drutopia source...
    echo "Updating drutopia_dev_template composer files"
    curl --output composer.json https://gitlab.com/drutopia/drutopia_dev_template/-/raw/main/composer.json
  elif [ "${which_composer}" == "standard" ]; then
    echo "Updating drupal-recommended composer files"
    . $AMANITA_DIR/.amanita-env
    latest_ver=$(curl -s https://api.github.com/repos/drupal/recommended-project/tags | jq -r '.[0].name')
    dl_ver=$recommended_project_ver
    if [ "$recommended_project_ver" != "$latest_ver" ]; then
      read -p "Latest version amanita knows is $latest_ver. Update $AMANITA_DIR/.amanita-env to use this (currently $recommended_project_ver)? (y/n): " answer
      # Check the result and take action accordingly
      case $answer in
          [yY]* )
              dl_ver=$latest_ver
              sed -i "s/recommended_project_ver=\"${recommended_project_ver}\"/recommended_project_ver=\"${latest_ver}\"/" $AMANITA_DIR/.amanita-env
              ;;
      esac
    fi
    curl --output composer.json https://raw.githubusercontent.com/drupal/recommended-project/${dl_ver}/composer.json
  else
    echo "Nothing to update, composer source is $which_composer"
  fi;

# Provide the instance (live/test) of the MayFirst account to pre-authenticate for ssh. Depends on .red_password file locally.
[no-cd]
mf-ssh-preauth instance:
  {{ justfile_dir() }}/mf-ssh-preauth.sh $PWD/.red_password {{ instance }}
