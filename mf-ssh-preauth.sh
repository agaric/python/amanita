#!/usr/bin/env bash

# Log into red control panel to establish two factor auth for ssh/sftp purposes.
# Pass one argument, which is the path to a file with the contents of your username
# and password separated by a colon, e.g.:
# USERNAME:PASSWORD

! which curl >/dev/null && printf "Please install curl.\n" && exit 1
! which jq >/dev/null && printf "Please install jq.\n" && exit 1

url=https://members.mayfirst.org/cp/api.php
password_file="$1"
instance="$2"

if [ -z "$password_file" ]; then
  printf "Please pass the path to your password file as the first argument.\n"
  exit 1
fi
if [ -z "$instance" ]; then
  printf "Please specify the instance you'd like to target (e.g. live/test).\n"
  exit 1
fi
if [ ! -f "$password_file" ]; then
  printf "Cannot find password file: %s.\n" "$password_file"
  printf "File contents should look like: 'instance:user:password' (w/out quotes)\n"
  exit 1
fi
user=$(grep "$instance:" "$password_file" | cut -d: -f2)
password=$(grep "$instance:" "$password_file" | cut -d: -f3)
if [ -z "$user" -o -z "$password" ]; then
  printf "Failed to locate user and password in password file.\n"
  exit 1
fi
out=$(curl --silent "${url}" -X POST -d "user_name=$user" -d "user_pass=$password" -d "action=grant")
exit=$(echo "$out" | jq .is_error)
if [ "$exit" != "0" ]; then
  echo "$out" | jq >&2
  exit 1
fi
echo "User authorized successfully." >&2
exit 0
